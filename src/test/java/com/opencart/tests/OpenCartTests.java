package com.opencart.tests;

import java.io.StringReader;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpenCartTests extends OCLandingPage {

	@Test
	public void tValidationitleTest() throws InterruptedException {
		openBrowser("firefox");
		navigate();
		// searchProduct(driver);
		String pageTitle = driver.getTitle();
		System.out.println(pageTitle);
		driver.findElement(By.xpath("//i[@class='fa fa-caret-down']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[text()='$ US Dollar']")).click();
		Thread.sleep(5000);
		List<WebElement> productsList = driver.findElements(By.xpath("//div[@class='product-thumb transition']"));
		for (int i = 0; i < productsList.size(); i++) {
			// String
			// Name=productsList.get(i).findElement(By.xpath("//div[@class='product-thumbtransition']/div[@class='caption']/h4/a")).getText();
			// WebElement product = productsList.get(i);
			// String name = product.findElement(By.xpath("//div[@class='product-thumb
			// transition']/div[@class='caption']/h4/a")).getText();
			// System.out.println(name);
			// System.out.println(name2);
			String name2;
			WebElement currentProd = null;
			try {
				name2 = productsList.get(i).getText();
			} catch (StaleElementReferenceException se) {
				// productsList = driver.findElements(By.xpath("//div[@class='product-thumb
				// transition']"));
				currentProd = driver.findElement(By.xpath("//div[@class='product-thumb transition'][" + i + "]"));
				name2 = currentProd.getText();
			}
			currentProd.findElement(By.xpath("//div[@class='product-thumb transition']/div[@class='caption']/h4/a"))
					.click();
			Thread.sleep(5000);
			String productname = driver.findElement(By.xpath("//div[@class='col-sm-4']/h1")).getText();
			String productprice = driver
					.findElement(By.xpath("//div[@class='col-sm-4']/ul[@class='list-unstyled']/li/h2")).getText();
			System.out.println(productname + "..." + productprice);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(5000);
			String lines[] = name2.split("\\r?\\n");
			System.out.println(lines[0]);
			System.out.println(lines[2]);

		}
	}

	@Test
	public void tValidationitleTest1() throws InterruptedException {
		String title = null;
		openBrowser("firefox");
		navigate();
		String pageTitle = driver.getTitle();
		System.out.println(pageTitle);
		driver.findElement(By.xpath("//i[@class='fa fa-caret-down']")).click();
		driver.findElement(By.xpath("//button[text()='$ US Dollar']")).click();
		explicitWaitElementClickable(driver, LOCATORS.getProperty("product.desktopElem_XPATH"));
		List<WebElement> productsList = driver.findElements(By.xpath("//div[@class='product-thumb transition']"));
		for (int i = 1; i <= productsList.size(); i++) {
			try {
				title = productsList.get(i - 1).getText();
			} catch (StaleElementReferenceException se) {
				productsList = driver.findElements(By.xpath("//div[@class='product-thumb transition']"));
				title = productsList.get(i - 1).getText();
			}
			System.out.println(getValue(title, 0));
			System.out.println(getValue(title, 2));
			WebElement elem = driver.findElement(By.cssSelector(
					"div#content>div.row>div:nth-child(" + i + ")>div.product-thumb.transition>div.image"));
			//Thread.sleep(2000);
			elem.click();
			explicitWaitElementClickable(driver, LOCATORS.getProperty("product.waitelement_XPATH"));
			//Thread.sleep(3000);
			String productname = driver.findElement(By.xpath("//div[@class='col-sm-4']/h1")).getText();
			String productprice = driver
					.findElement(By.xpath("//div[@class='col-sm-4']/ul[@class='list-unstyled']/li/h2")).getText();
			System.out.println(productname + "..." + productprice);
			driver.navigate().back();
			Assert.assertTrue(containsValue(getValue(title, 0), productname), "Product Name is Not Matching");
			Assert.assertTrue(containsValue(getValue(title, 2), productprice), "Product Price is Not Matching");
		}

	}

	public String getValue(String text, int line) {
		String lines[] = text.split("\\r?\\n");
		return lines[line];

	}

	public boolean containsValue(String st, String dest) {
		if (st.contains(dest)) {
			return true;
		} else {
			return false;
		}
	}
}
