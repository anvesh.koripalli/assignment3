package com.opencart.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.main.base.SeleniumBase;

public class OCLandingPage extends SeleniumBase {

	public void searchProduct(WebDriver driver) {
		String productTitle = null;
		mouseOver(driver, LOCATORS.getProperty("search.main.menu_XPATH"));
		click(driver, LOCATORS.getProperty("search.main.menu_option_XPATH"));
		List<WebElement> products = findElements(driver, LOCATORS.getProperty("land.products_XPATH"));
		for (WebElement webElement : products) {
			//productTitle = webElement.findElement(By.xpath(LOCATORS.getProperty("product.name_XPATH"))).getText();
			//System.out.println(productTitle);
		}
		
		for(int i=0; i<products.size(); i++) {
			String s = products.get(i).getText();
			System.out.println(s);
		}
	}
	
	public void pageVerification(WebDriver driver) {
		
	}

}
