package com.search.test;

import org.openqa.selenium.WebDriver;

import com.main.base.GridConfig;
import com.main.base.ParellelRunGridBase;

public class FLHomePage extends ParellelRunGridBase {
	
	public void searchProduct(WebDriver driver, String data) {
		click(getDriver(), LOCATORS.getProperty("loginClose_XPATH"));
		sendKeys(getDriver(), LOCATORS.getProperty("searchPageField_XPATH"), data);
		click(getDriver(), LOCATORS.getProperty("searchTab_XPATH"));
	}
}
