package com.search.test;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TitlevalidateTest extends FLHomePage {

	@Test(dataProvider = "getBrowserDetails")
	public void tValidationitleTest(String browser) {
		System.out.println("INFO:----------------------Initiating " + browser + " browser");
		openBrowser(browser);
		System.out.println("INFO " + browser + " Initiated");
		navigate();
		searchProduct(getDriver(), "laptop");
		explicitWaitElementClickable(getDriver(), LOCATORS.getProperty("search.page.tab_XPATH"));
		/*
		 * try { Thread.sleep(5000); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 */
		Assert.assertEquals(getPageTitle(getDriver()), LOCATORS.getProperty("page.title"));

	}

	/*
	 * @Test
	 * 
	 * public void titleTest() { openBrowser("chrome"); navigate();
	 * searchProduct(driver, "laptop"); Assert.assertEquals(getPageTitle(driver),
	 * LOCATORS.getProperty("page.title")); }
	 */

	@DataProvider(parallel = true)
	public Object[][] getBrowserDetails() {
		Object[][] data = new Object[2][1];
		data[0][0] = LOCATORS.getProperty("data.xcl.browser1");
		data[1][0] = LOCATORS.getProperty("data.xcl.browser2");
		return data;
	}

	@AfterMethod
	public void tearDown() {
		if (getDriver() != null) {
			// driver.close();
			getDriver().quit();
		}
	}
}
