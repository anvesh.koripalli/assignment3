package com.main.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeSuite;

public class GridConfig {

	public static WebDriver driver;
	public static Properties LOCATORS = new Properties();
	public static FileInputStream fis;
	String projectLocation = System.getProperty("user.dir");

	@BeforeSuite
	public void setup() {
		if (driver == null) {
			try {
				fis = new FileInputStream(projectLocation + "/src/main/resources/config/locators.properties");
				LOCATORS.load(fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void openBrowser(String browser) {
		DesiredCapabilities capabilities = null;
		if (browser.equalsIgnoreCase("chrome")) {
			capabilities = DesiredCapabilities.chrome();
			capabilities.setBrowserName("chrome");
			capabilities.setPlatform(Platform.ANY);
		} else if (browser.equalsIgnoreCase("firefox")) {
			capabilities = DesiredCapabilities.firefox();
			capabilities.setBrowserName("firefox");
			capabilities.setPlatform(Platform.ANY);
		}
		try {
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
	}

	public void navigate() {
		driver.get(LOCATORS.getProperty("site.url"));
	}

	public String getPageTitle(WebDriver driver) {
		String title = null;
		try {
			title = driver.getTitle();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return title;
	}

	public static WebElement findElement(WebDriver driver, String objectLocator) {

		try {
			String[] splits = objectLocator.split("~");
			String objecttype = splits[0];
			String objectvalue = splits[1];
			if (objecttype.equalsIgnoreCase("id")) {
				return driver.findElement(By.id(objectvalue));
			} else if (objecttype.equalsIgnoreCase("xpath")) {
				return driver.findElement(By.xpath(objectvalue));
			} else if (objecttype.equalsIgnoreCase("name")) {
				return driver.findElement(By.name(objectvalue));
			} else if (objecttype.equalsIgnoreCase("classname")) {
				return driver.findElement(By.className(objectvalue));
			} else if (objecttype.equalsIgnoreCase("tagname")) {
				return driver.findElement(By.tagName(objectvalue));
			} else if (objecttype.equalsIgnoreCase("css")) {
				return driver.findElement(By.cssSelector(objectvalue));
			} else if (objecttype.equalsIgnoreCase("linkText")) {
				return driver.findElement(By.linkText(objectvalue));
			} else if (objecttype.equalsIgnoreCase("partialLinkText")) {
				return driver.findElement(By.partialLinkText(objectvalue));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void click(WebDriver driver, String objectLocator) {
		try {
			if (findElement(driver, objectLocator).isEnabled()) {
				findElement(driver, objectLocator).click();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendKeys(WebDriver driver, String objectLocator, String value) {
		try {
			findElement(driver, objectLocator).clear();
			findElement(driver, objectLocator).sendKeys(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void selectDropdown(WebDriver driver, String objectLocator, String value) {
		try {
			new Select(findElement(driver, objectLocator)).selectByVisibleText(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
