package com.main.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumBase {

	public static WebDriver driver;
	public static Properties LOCATORS = new Properties();
	public static FileInputStream fis;
	String projectLocation = System.getProperty("user.dir");

	@BeforeSuite
	public void setup() {
		if (driver == null) {
			try {
				fis = new FileInputStream(projectLocation + "/src/main/resources/config/locators.properties");
				LOCATORS.load(fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void openBrowser(String browser) {
		DesiredCapabilities cap = null;
		if (browser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
	}

	public void navigate() {
		driver.get(LOCATORS.getProperty("site.url"));
	}

	public static WebDriver getDriverInstance() {
		return driver;
	}

	public String getPageTitle(WebDriver driver) {
		String title = null;
		try {
			title = driver.getTitle();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return title;
	}

	public static WebElement findElement(WebDriver driver, String objectLocator) {

		try {
			String[] splits = objectLocator.split("~");
			String objecttype = splits[0];
			String objectvalue = splits[1];
			if (objecttype.equalsIgnoreCase("id")) {
				return driver.findElement(By.id(objectvalue));
			} else if (objecttype.equalsIgnoreCase("xpath")) {
				return driver.findElement(By.xpath(objectvalue));
			} else if (objecttype.equalsIgnoreCase("name")) {
				return driver.findElement(By.name(objectvalue));
			} else if (objecttype.equalsIgnoreCase("classname")) {
				return driver.findElement(By.className(objectvalue));
			} else if (objecttype.equalsIgnoreCase("tagname")) {
				return driver.findElement(By.tagName(objectvalue));
			} else if (objecttype.equalsIgnoreCase("css")) {
				return driver.findElement(By.cssSelector(objectvalue));
			} else if (objecttype.equalsIgnoreCase("linkText")) {
				return driver.findElement(By.linkText(objectvalue));
			} else if (objecttype.equalsIgnoreCase("partialLinkText")) {
				return driver.findElement(By.partialLinkText(objectvalue));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<WebElement> findElements(WebDriver driver, String objectLocator) {

		try {
			String[] splits = objectLocator.split("~");
			String objecttype = splits[0];
			String objectvalue = splits[1];
			if (objecttype.equalsIgnoreCase("id")) {
				return driver.findElements(By.id(objectvalue));
			} else if (objecttype.equalsIgnoreCase("xpath")) {
				return driver.findElements(By.xpath(objectvalue));
			} else if (objecttype.equalsIgnoreCase("name")) {
				return driver.findElements(By.name(objectvalue));
			} else if (objecttype.equalsIgnoreCase("classname")) {
				return driver.findElements(By.className(objectvalue));
			} else if (objecttype.equalsIgnoreCase("tagname")) {
				return driver.findElements(By.tagName(objectvalue));
			} else if (objecttype.equalsIgnoreCase("css")) {
				return driver.findElements(By.cssSelector(objectvalue));
			} else if (objecttype.equalsIgnoreCase("linkText")) {
				return driver.findElements(By.linkText(objectvalue));
			} else if (objecttype.equalsIgnoreCase("partialLinkText")) {
				return driver.findElements(By.partialLinkText(objectvalue));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void mouseOver(WebDriver driver, String ObjectLocator) {
		Actions action = new Actions(driver);
		action.moveToElement(findElement(driver, ObjectLocator));
		action.build().perform();
	}

	public static void click(WebDriver driver, String objectLocator) {
		try {
			if (findElement(driver, objectLocator).isEnabled()) {
				findElement(driver, objectLocator).click();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendKeys(WebDriver driver, String objectLocator, String value) {
		try {
			findElement(driver, objectLocator).clear();
			findElement(driver, objectLocator).sendKeys(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void selectDropdown(WebDriver driver, String objectLocator, String value) {
		try {
			new Select(findElement(driver, objectLocator)).selectByVisibleText(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void explicitWaitElementClickable(WebDriver driver, String objectLocator) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.elementToBeClickable(findElement(driver, objectLocator)));
		} catch (Exception e) {
		}

	}
}
