package com.main.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;

public class ParellelRunGridBase {

	public WebDriver driver;
	public static ThreadLocal<WebDriver> dr = new ThreadLocal<WebDriver>();
	public static Properties LOCATORS = new Properties();
	public static FileInputStream fis;
	String projectLocation = System.getProperty("user.dir");

	@BeforeSuite
	public void setup() {
		if (driver == null) {
			try {
				fis = new FileInputStream(projectLocation + "/src/main/resources/config/locators.properties");
				LOCATORS.load(fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void openBrowser(String browser) {
		DesiredCapabilities capabilities = null;
		if (browser.equalsIgnoreCase("chrome")) {
			capabilities = DesiredCapabilities.chrome();
			capabilities.setBrowserName("chrome");
			capabilities.setPlatform(Platform.ANY);
		} else if (browser.equalsIgnoreCase("firefox")) {
			capabilities = DesiredCapabilities.firefox();
			capabilities.setBrowserName("firefox");
			capabilities.setPlatform(Platform.ANY);
		}
		try {
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		configureDriver(driver);
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
	}

	public void configureDriver(WebDriver driver) {
		dr.set(driver);
	}

	public WebDriver getDriver() {
		return dr.get();
	}

	public void navigate() {
		getDriver().get(LOCATORS.getProperty("site.url"));
	}

	public String getPageTitle(WebDriver driver) {
		String title = null;
		try {
			title = getDriver().getTitle();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return title;
	}

	public WebElement findElement(WebDriver driver, String objectLocator) {

		try {
			String[] splits = objectLocator.split("~");
			String objecttype = splits[0];
			String objectvalue = splits[1];
			if (objecttype.equalsIgnoreCase("id")) {
				return getDriver().findElement(By.id(objectvalue));
			} else if (objecttype.equalsIgnoreCase("xpath")) {
				return getDriver().findElement(By.xpath(objectvalue));
			} else if (objecttype.equalsIgnoreCase("name")) {
				return getDriver().findElement(By.name(objectvalue));
			} else if (objecttype.equalsIgnoreCase("classname")) {
				return getDriver().findElement(By.className(objectvalue));
			} else if (objecttype.equalsIgnoreCase("tagname")) {
				return getDriver().findElement(By.tagName(objectvalue));
			} else if (objecttype.equalsIgnoreCase("css")) {
				return getDriver().findElement(By.cssSelector(objectvalue));
			} else if (objecttype.equalsIgnoreCase("linkText")) {
				return getDriver().findElement(By.linkText(objectvalue));
			} else if (objecttype.equalsIgnoreCase("partialLinkText")) {
				return getDriver().findElement(By.partialLinkText(objectvalue));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void click(WebDriver driver, String objectLocator) {
		try {
			if (findElement(getDriver(), objectLocator).isEnabled()) {
				findElement(getDriver(), objectLocator).click();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendKeys(WebDriver driver, String objectLocator, String value) {
		try {
			findElement(getDriver(), objectLocator).clear();
			findElement(getDriver(), objectLocator).sendKeys(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void explicitWaitElementClickable(WebDriver driver, String objectLocator) {
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(), 15);
			wait.until(ExpectedConditions.elementToBeClickable(findElement(getDriver(), objectLocator)));
		} catch (Exception e) {
		}

	}

}
